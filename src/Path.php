<?php

declare (strict_types = 1);

namespace M\Php;

use Exception;
use M\Php\Exceptions\MoveDoesNotValidException;
use M\Php\Helpers\MoveHelper;
use M\Php\Helpers\PathHelper;

class Path
{
    /**
     * @var string
     */
    public $currentPath;

    /**
     * @var Mover
     */
    private $mover;

    /**
     * @param string $path
     */
    public function __construct(
        private string $path
    ) {
        if (!PathHelper::isAbsolutePath(substr($path, 0, 1))) {
            throw new Exception;
        }

        $this->mover       = new Mover();
        $this->currentPath = $path;
    }

    /**
     * @param  string      $destination
     * @throws Exception
     */
    public function cd(string $destinationPath): void
    {
        if (!MoveHelper::isValidMove($destinationPath)) {
            throw new MoveDoesNotValidException();
        }

        $this->mover->move($this->currentPath, $destinationPath);
    }
}
