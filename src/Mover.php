<?php

declare (strict_types = 1);

namespace M\Php;

use M\Php\Helpers\PathHelper;
use M\Php\Utils\PathUtil;

class Mover
{
    /**
     * @var array
     */
    private $levelPaths;

    /**
     * @var string
     */
    private $currentPath;

    /**
     * @param  string &$currentPath
     * @param  string $destinationPath
     * @return void
     */
    public function move(
        string &$currentPath,
        string  $destinationPath
    ): void{
        $this->syncLevelPaths($currentPath);

        $this->handle($currentPath, $destinationPath);

        $this->syncLevelPaths($currentPath);
    }

    /**
     * @param  string &$currentPath
     * @param  string $destinationPath
     * @return void
     */
    private function handle(
        string &$currentPath,
        string  $destinationPath
    ): void {
        if (PathHelper::isAbsolutePath($destinationPath)) {
            $currentPath = PathUtil::SEPARATOR;
        } else {
            $destinationPath = PathHelper::normalizeAsArray($destinationPath);
            foreach ($destinationPath as $path) {
                switch ($path) {
                    case '.':
                        break;
                    case '..':
                        $currentPath = $this->moveToPreviosDir();
                        break;
                    default:
                        $currentPath = $this->moveToNext($currentPath, $path);
                }
            }
        }
    }

    /**
     * @param  string   $currentPath
     * @param  string   $destinationPath
     * @return string
     */
    private function moveToNext(
        string $currentPath,
        string $destinationPath
    ): string {
        return $currentPath . PathUtil::SEPARATOR . $destinationPath;
    }

    /**
     * @return string
     */
    private function moveToPreviosDir(): string
    {
        array_pop($this->levelPaths);

        return PathUtil::SEPARATOR . join(PathUtil::SEPARATOR, $this->levelPaths);
    }

    /**
     * @param  string $currentPath
     * @return void
     */
    private function syncLevelPaths(string $currentPath): void
    {
        $this->levelPaths = array_filter(explode(PathUtil::SEPARATOR, $currentPath));
    }
}
