<?php

declare (strict_types = 1);

namespace M\Php\Helpers;

class MoveHelper
{
    /**
     * @param  string    $destinationPath
     * @return boolean
     */
    public static function isValidMove(string $destinationPath): bool
    {
        $destinationPath = PathHelper::normalizeAsArray($destinationPath);

        foreach ($destinationPath as $key => $path) {
            /**
             * first condition needed to verify if exists more then two .
             * second condition verify that path it's a command to move-back and not others.
             */
            if (substr_count($path, '.') > 2 && substr_count($path, '.') === strlen($path)) {
                return false;
            }

            if (!preg_match("/[a-zA-Z]+/", $path) && !in_array($path, ['.', '..'])) {
                return false;
            }
        }

        return true;
    }
}
