<?php

declare (strict_types = 1);

namespace M\Php\Helpers;

use M\Php\Utils\PathUtil;

class PathHelper
{
    /**
     * @return array
     */
    public static function normalizeAsArray(string $path): array
    {
        return array_filter(explode(PathUtil::SEPARATOR, $path));
    }

    /**
     * @return boolean
     */
    public static function isAbsolutePath(string $path): bool
    {
        return PathUtil::SEPARATOR === $path;
    }

}
