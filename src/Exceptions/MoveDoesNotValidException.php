<?php

declare(strict_types=1);

namespace M\Php\Exceptions;

use Exception;

class MoveDoesNotValidException extends Exception
{
}
