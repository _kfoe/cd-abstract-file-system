<?php

namespace M\Php\Tests;

use M\Php\Exceptions\MoveDoesNotValidException;
use M\Php\Path;

class PathTest extends \PHPUnit\Framework\TestCase
{
    /** @test */
    public function it_valid_stay()
    {
        $path = new Path('/a/b/c/d');
        $path->cd('.');
        $this->assertEquals($path->currentPath, '/a/b/c/d');

        $path = new Path('/a/b/c/d');
        $path->cd('./');
        $this->assertEquals($path->currentPath, '/a/b/c/d');
    }

    /** @test */
    public function it_valid_move_back()
    {
        $path = new Path('/a/b/c/d');
        $path->cd('../');
        $this->assertEquals($path->currentPath, '/a/b/c');
        $path->cd('../');
        $this->assertEquals($path->currentPath, '/a/b');
        $path->cd('../');
        $this->assertEquals($path->currentPath, '/a');
        $path->cd('../');
        $this->assertEquals($path->currentPath, '/');
    }

    /** @test */
    public function it_valid_back_to_absolute_one_shot()
    {
        $path = new Path('/a/b/c/d');
        $path->cd('/');
        $this->assertEquals($path->currentPath, '/');
    }

    /** @test */
    public function it_valid_complex_handle()
    {
        $path = new Path('/a/b/c/d');
        $path->cd('././////../x');
        $this->assertEquals($path->currentPath, '/a/b/c/x');
    }

    /** @test */
    public function it_valid_back_and_move_ahead()
    {
        $path = new Path('/a/b/c/d');
        $path->cd('../x');
        $this->assertEquals($path->currentPath, '/a/b/c/x');
    }

    /** @test */
    public function it_valid_move_ahead()
    {
        $path = new Path('/a/b/c/d');
        $path->cd('./x/c/y');
        $this->assertEquals($path->currentPath, '/a/b/c/d/x/c/y');
    }

    /** @test */
    public function it_valid_move_ahead_2()
    {
        $path = new Path('/a/b/c/d');
        $path->cd('x/c/y');
        $this->assertEquals($path->currentPath, '/a/b/c/d/x/c/y');
    }

    /** @test */
    public function it_throw_move_does_not_valid_exception()
    {
        $this->expectException(MoveDoesNotValidException::class);

        $path = new Path('/a/b/c/d');
        $path->cd('./?(&%$*/x/c/y');
    }
}
